﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Azzysa.EF.SmarTeam;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.SmarTeamServices.Controllers
{

    /// <summary>
    /// API controller that serves Exchange Framework processing functinonality based on SmarTeam
    /// </summary>
    public class ExchangeFrameworkController : ApiController,
        IExtendableController
    {

        /// <summary>
        /// Processing data package on the SmarTeam platform
        /// </summary>
        /// <param name="package">Datapackage containing the mapping (reference) and objects to process</param>
        /// <returns><see cref="DataPackage"/> object containing process results</returns>
        [HttpPost]
        public DataPackage Process(DataPackage package)
        {
            // Create return package (contains all objects, with process information, that failed during the import)
            var returnPackage = new DataPackage()
            {
                PackageId = package.PackageId,
                Mapping = package.Mapping,
                MappingReference = package.MappingReference
            };

            // Parse the package with its objects according to the specified mapping
            try
            {
                // Get connection SmarTeam Api and get session
                using (
                    var connection =
                        new Infostrait.Azzysa.EV5ST.ConnectionPool.Connections.EV5STConnection(
                            this.GetSystemConnectionString()))
                {
                    // Open the connection with SmarTeam
                    connection.Open();

                    // Get and check SmarTeamServices config directory (relative to script directory)
                    var configDirectory =
                        System.IO.Path.Combine(
                            connection.Session.Config.get_Value("$Admin\\Directory Structure\\ScriptDirectory"),
                            "SmarTeamServices");

                    // Determine mapping, only package.MappingReference is supported for now!
                    var mapping = package.RetrieveMapping(configDirectory);

                    // Sort objects accordingly the sort expression
                    List<DataObject> dataObjects;
                    if (!string.IsNullOrWhiteSpace(mapping.SortAttribute))
                    {
                        if (mapping.SortAttribute.ToLower().StartsWith("smarteamrevisionsort"))
                        {
                            // SmarTeam revision sort
                            var originAttributeName = mapping.SortAttribute.Substring(mapping.SortAttribute.IndexOf("[", StringComparison.InvariantCulture) + 1, (mapping.SortAttribute.IndexOf(";", StringComparison.InvariantCulture) - (mapping.SortAttribute.IndexOf("[", StringComparison.InvariantCulture) + 1)));
                            var revisionAttributeName = mapping.SortAttribute.Substring(mapping.SortAttribute.IndexOf(";", StringComparison.InvariantCulture) + 1, (mapping.SortAttribute.LastIndexOf(";", StringComparison.InvariantCulture) - (mapping.SortAttribute.IndexOf(";", StringComparison.InvariantCulture) + 1)));
                            var parentRevisionAttributeName = mapping.SortAttribute.Substring(mapping.SortAttribute.LastIndexOf(";", StringComparison.InvariantCulture) + 1, (mapping.SortAttribute.LastIndexOf("]", StringComparison.InvariantCulture) - (mapping.SortAttribute.LastIndexOf(";", StringComparison.InvariantCulture) + 1)));

                            dataObjects = package.DataObjects.GetObjectsSmarTeamRevisionSorted(originAttributeName, revisionAttributeName, parentRevisionAttributeName);
                        }
                        else if (mapping.SortAttribute.ToLower().StartsWith("smarteamcreationdatesort"))
                        {
                            // SmarTeam revision/creation date sort
                            var s = mapping.SortAttribute.Substring(mapping.SortAttribute.IndexOf("[", StringComparison.InvariantCultureIgnoreCase) + 1);
                            s = s.Substring(0, s.Length - 1);
                            var attrs = s.Split(new char[] { Convert.ToChar(";") }, StringSplitOptions.RemoveEmptyEntries);
                            var originAttributeName = attrs[0];
                            var creationDateAttributeName = attrs[1];
                            var revisionAttributeName = attrs[2];
                            var parentRevisionAttributeName = attrs[3];
                           
                            dataObjects = package.DataObjects.GetObjectsSortedOnCreationDate(originAttributeName, creationDateAttributeName, revisionAttributeName, parentRevisionAttributeName);
                        }
                        else {
                            // Attribute sort
                            dataObjects = package.DataObjects.GetObjectsSorted(mapping.SortAttribute);
                        }
                    }
                    else
                    {
                        // Do not sort
                        dataObjects = package.DataObjects.Collection.Collection;
                    }

                    // Iterate through data object collection
                    foreach (var dataObject in dataObjects)
                    {
                        // Process object
                        var invocationResult = dataObject.Process(connection.Session, mapping);

                        // Check result, and update the data object
                        if (invocationResult == null)
                        {
                            // Unexpected invocation result
                            throw new ApplicationException("dataObject.Process returned Null as invocation result!");
                        }

                        // Process the invocation results
                        switch (invocationResult.Result)
                        {
                            case ProcessResult.eResult.Error:
                                returnPackage.SetObjectProcessedError(dataObject, invocationResult.ExceptionText, invocationResult.ExceptionMessage);
                                break;
                            case ProcessResult.eResult.Warning:
                                returnPackage.SetObjectProcessedWarning(dataObject, invocationResult.ExceptionText, invocationResult.ExceptionMessage);
                                break;
                            case ProcessResult.eResult.Success:
                                returnPackage.SetObjectProcessedSuccess(dataObject, invocationResult.ExceptionText, invocationResult.ExceptionMessage);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Unexpected exception occured
                returnPackage.SetObjectProcessedError(new DataObject(), ex.ToString(), ex.Message);
            }

            // Return the returnPackage object containing process results
            return returnPackage;
        }
    }
}