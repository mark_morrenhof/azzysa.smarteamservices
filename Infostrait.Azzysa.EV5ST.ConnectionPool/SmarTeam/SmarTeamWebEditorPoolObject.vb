''' <summary>
''' Pooled object, living in the pool. Contains a connection with the SmarTeam API
''' and specific libraries to perform actions in SmarTeam.
'''
''' Uses one SmarTeam Web Editor license.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[t.v.oost]	11-5-2012	Created
''' </history>
Public Class SmarTeamWebEditorPoolObject
    Inherits SmarTeamBasePoolObject

    Protected Overrides ReadOnly Property Mode() As SmarTeamPoolObjectMode
        Get
            Return SmarTeamBasePoolObject.SmarTeamPoolObjectMode.WebEditor
        End Get
    End Property

    Public Sub New(connectionString As Dictionary(Of String, String))
        MyBase.New(connectionString)
    End Sub
End Class