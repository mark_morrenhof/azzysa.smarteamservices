Imports System.Configuration.ConfigurationManager
Imports System.Threading

''' <summary>
''' Use this class to host the connection pool.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[t.v.oost]	15-10-2007	Created
''' </history>
Public Class HostPool
    Private Sub New()
    End Sub

    Public Shared WithEvents Instance As HostPool = New HostPool

    ''' <summary>
    ''' Call this function to host the pool in the current application.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	15-10-2007	Created
    ''' </history>
    Public Sub Initiate()
        Interlocked.Exchange(_running, 1)

        Dim init As Thread
        init = New Thread(AddressOf InitiatePoolManager)
        init.Start()
    End Sub

    Public Sub Terminate()
        Interlocked.Exchange(_running, 0)

        ' Terminate pool manager
        If PoolManager.Instance IsNot Nothing Then
            PoolManager.Instance.TerminatePool()
        End If
    End Sub

#Region " Private "
    Private Shared _running As Int32 = 0

    Private Sub InitiatePoolManager()
      ' Initiate pool manager
        Try
            PoolManager.Instance.Initiate(AppSettings("ConnectionString"))
        Finally
            Dim validate As Thread
            validate = New Thread(AddressOf ValidatePool)
            validate.Start()
        End Try
    End Sub

    Private Sub ValidatePool()
        ' Validate the entire pool every time period
        Dim validationSleepMinutes = 30

        If Not String.IsNullOrEmpty(AppSettings("PoolValidationTimeOutInMinutes")) Then
            validationSleepMinutes = Integer.Parse(AppSettings("PoolValidationTimeOutInMinutes"))
        End If

        While _running = 1
            Try
                PoolManager.Instance.ValidatePoolBounds()
                PoolManager.Instance.ValidateObjects()
            Finally
                Thread.Sleep(TimeSpan.FromMinutes(validationSleepMinutes))
            End Try
        End While
    End Sub

#End Region
End Class