﻿namespace Azzysa.EF.SmarTeam
{
    public class ProcessResult
    {

        public ProcessResult()
        {
            // Default constructor
        }

        public enum eResult
        {
            Success,
            Warning,
            Error
        }

        public eResult Result;

        public string ExceptionText;

        public string ExceptionMessage;

    }
}