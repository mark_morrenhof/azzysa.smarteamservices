Imports System.Collections
Imports System.Text
Imports System.Threading

Namespace StaThreadSynchronizer
    Friend Interface IQueueReader
        Inherits IDisposable
        Function Dequeue() As Object
        Sub ReleaseReader()
    End Interface

    Friend Interface IQueueWriter
        Inherits IDisposable
        Sub Enqueue(data As Object)
    End Interface

    Friend Class BlockingQueue
        Implements IQueueReader
        Implements IQueueWriter
        Implements IDisposable
        Private mQueue As New Queue
        Private mLocker As Semaphore
        Private mKillThread As New ManualResetEvent(False)
        Private mWaitHandles As WaitHandle()

        Public Sub New()
            mLocker = New Semaphore(0, Integer.MaxValue)
            mWaitHandles = New WaitHandle(1) {mLocker, mKillThread}
        End Sub

        Public Sub Enqueue(data As Object) Implements IQueueWriter.Enqueue
            SyncLock mQueue
                mQueue.Enqueue(data)
            End SyncLock
            mLocker.Release()
        End Sub

        Public Function Dequeue() As Object Implements IQueueReader.Dequeue
            WaitHandle.WaitAny(mWaitHandles)
            SyncLock mQueue
                If mQueue.Count > 0 Then
                    Return mQueue.Dequeue()
                End If
            End SyncLock
            Return Nothing
        End Function

        Public Sub ReleaseReader() Implements IQueueReader.ReleaseReader
            mKillThread.[Set]()
        End Sub

        Private Sub IDisposable_Dispose() Implements IDisposable.Dispose
            If Not mLocker Is Nothing Then
                mLocker.Close()
                mLocker = Nothing
            End If
        End Sub
    End Class
End Namespace
