﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infostrait.Azzysa.EF.Common;
using SmarTeam.Std.Interop.SmarTeam.SmApplic;

namespace Azzysa.EF.SmarTeam
{
    public static class ParametersExtensions
    {
        public static object[] GetValues(this List<Parameter> collection, ISmSession session, DataObject dataObject)
        {
            var returnValues = new List<object>();

            // Sort the parameters and iterate
            collection.Sort(new ParameterComparator());
            foreach (var parameter in collection)
            {
                // Check parameter type requested
                if (string.Equals(parameter.Type, typeof (ISmSession).FullName,
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    // SmarTeam session requested
                    returnValues.Add(session);
                }
                else if (string.IsNullOrWhiteSpace(parameter.Value) && parameter.FunctionResult != null)
                {
                    // Function result
                    throw new NotImplementedException("Parameter type FunctionResult is not supported.");
                }
                else if (string.IsNullOrWhiteSpace(parameter.Value) && parameter.TypeInvocation != null)
                {
                    // Type invocation result
                    returnValues.Add(parameter.TypeInvocation.DoCollectInvocationResult(session, dataObject));
                }
                else if (parameter.Value.StartsWith("${ATTRIBUTES", StringComparison.InvariantCultureIgnoreCase) && parameter.Value.EndsWith("}", 
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    // Attribute list
                    throw new NotImplementedException("Parameter type AttributeList is not supported.");
                }
                else if (parameter.Value.StartsWith("$DATA_OBJECT", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Set the entire data object as a parameter
                    returnValues.Add(dataObject);
                }
                else if (parameter.Value.StartsWith("{", StringComparison.InvariantCultureIgnoreCase) && parameter.Value.EndsWith("}",
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    // Use object value
                    var sAttributeName = parameter.Value.Substring(1);
                    sAttributeName = sAttributeName.Substring(0, sAttributeName.Length - 1);

                    // Check if attribute exists
                    if (
                        dataObject.Attributes.Exists(
                            p => string.Equals(p.Name, sAttributeName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        // Attribute exists, add it to the return collection
                        returnValues.Add(
                            dataObject.Attributes.Single(
                                p => string.Equals(p.Name, sAttributeName, StringComparison.InvariantCultureIgnoreCase))
                                .Value);
                    }
                    else
                    {
                        // Attribute does not exist, add null value
                        returnValues.Add(null);
                    }
                }
                else if (string.Equals("${NULL}", parameter.Value, StringComparison.InvariantCultureIgnoreCase))
                {
                    // Set Null value
                    returnValues.Add(null);
                }
                else
                {
                    // Use literal text
                    returnValues.Add(parameter.Value);
                }
            }

            // Finished
            return returnValues.ToArray();
        }
    }
}