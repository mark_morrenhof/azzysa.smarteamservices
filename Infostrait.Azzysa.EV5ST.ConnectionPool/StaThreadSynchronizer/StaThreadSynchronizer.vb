Imports System.Text
Imports System.Threading

Namespace StaThreadSynchronizer
    Public Class StaSynchronizationContext
        Implements IDisposable

        Private mQueue As BlockingQueue
        Private mStaThread As StaThread

        Public Sub New()
            MyBase.New()
            mQueue = New BlockingQueue
            mStaThread = New StaThread(mQueue)
            mStaThread.Start()
        End Sub

        Public Sub Send(d As SendOrPostCallback, state As Object)
            ' to avoid deadlock!
            If Threading.Thread.CurrentThread.GetHashCode() = mStaThread.GetHashCode() Then
                d(state)
                Return
            End If

            ' create an item for execution
            Dim item As New SendOrPostCallbackItem(d, state, ExecutionType.Send)
            ' queue the item
            mQueue.Enqueue(item)
            ' wait for the item execution to end
            item.ExecutionCompleteWaitHandle.WaitOne()

            ' if there was an exception, throw it on the caller thread, not the
            ' sta thread.
            If item.ExecutedWithException Then
                Throw item.Exception
            End If
        End Sub

        Public Sub Post(d As SendOrPostCallback, state As Object)
            ' queue the item and don't wait for its execution. This is risky because
            ' an unhandled exception will terminate the STA thread. Use with caution.
            Dim item As New SendOrPostCallbackItem(d, state, ExecutionType.Post)
            mQueue.Enqueue(item)
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            mStaThread.[Stop]()
        End Sub

        Public Function CreateCopy() As StaSynchronizationContext
            Return Me
        End Function
    End Class
End Namespace
