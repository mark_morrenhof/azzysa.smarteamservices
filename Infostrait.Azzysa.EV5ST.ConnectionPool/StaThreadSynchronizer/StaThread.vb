Imports System.Text
Imports System.Threading

Namespace StaThreadSynchronizer
    Friend Class StaThread
        Private mStaThread As Thread
        Private mQueueConsumer As IQueueReader
        Private mStopEvent As New ManualResetEvent(False)
        Private mId As Integer = 0

        Private Shared mCounter As Integer = 0

        Friend Sub New(reader As IQueueReader)
            mId = Interlocked.Increment(mCounter)

            mQueueConsumer = reader
            mStaThread = New Thread(AddressOf Run)
            mStaThread.Name = "STA Worker #" & mId.ToString
            mStaThread.SetApartmentState(ApartmentState.STA)
        End Sub

        Public Overrides Function GetHashCode() As Integer
            Return mStaThread.GetHashCode()
        End Function

        Friend Sub Start()
            mStaThread.Start()
        End Sub

        Friend Sub Join()
            mStaThread.Join()
        End Sub

        Private Sub Run()
            While True
                Dim [stop] As Boolean = mStopEvent.WaitOne(0, False)
                If [stop] Then
                    Exit While
                End If

                Dim workItem As SendOrPostCallbackItem = DirectCast(mQueueConsumer.Dequeue(), SendOrPostCallbackItem)
                If Not workItem Is Nothing Then
                    workItem.Execute()
                End If
            End While
        End Sub

        Friend Sub [Stop]()
            mStopEvent.[Set]()
            mQueueConsumer.ReleaseReader()
            mStaThread.Join()
            mQueueConsumer.Dispose()
        End Sub
    End Class
End Namespace