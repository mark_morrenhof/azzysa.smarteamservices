﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public static class DataPackageExtensions
    {

        private static void SetObjectStatus(DataPackage returnPackage, DataObject dataObject, string errorText,
            string errorMessage, string objectStatus)
        {
            if (dataObject != null)
            {
                // Create error attributes
                var attr = new ObjectAttribute()
                {
                    Name = "ResultCode",
                    Value = objectStatus
                };
                dataObject.Attributes.Add(attr);

                // Try to prevent empty error message (may occur when re-throwing exceptions
                attr = new ObjectAttribute()
                {
                    Name = "ErrorMessage",
                    Value = errorMessage
                };
                dataObject.Attributes.Add(attr);

                attr = new ObjectAttribute()
                {
                    Name = "ErrorText",
                    Value = errorText
                };
                dataObject.Attributes.Add(attr);

                // Check if data object is needed to be added to the return package
                returnPackage?.DataObjects.Add(dataObject);
            }
        }

        public static void SetObjectProcessedError(this DataPackage returnPackage, DataObject dataObject, string errorText, string errorMessage)
        {
            SetObjectStatus(returnPackage, dataObject, errorText, errorMessage, "Error");
        }

        public static void SetObjectProcessedWarning(this DataPackage returnPackage, DataObject dataObject, string errorText, string errorMessage)
        {
            SetObjectStatus(returnPackage, dataObject, errorText, errorMessage, "Warning");
        }

        public static void SetObjectProcessedSuccess(this DataPackage returnPackage, DataObject dataObject, string errorText, string errorMessage)
        {
            SetObjectStatus(returnPackage, dataObject, errorText, errorMessage, "Success");
        }

        public static Mapping RetrieveMapping(this DataPackage package, string configurationPath)
        {
            // Check mapping reference (only package.MappingReference is supported for now)
            if (string.IsNullOrEmpty(package.MappingReference))
            {
                throw new ApplicationException("No mapping reference, unable to process package!");
            }

            // Check configuration path parameter
            if (string.IsNullOrWhiteSpace(configurationPath)) {  throw new ArgumentNullException(nameof(configurationPath));}
            if (!Directory.Exists(configurationPath)) { throw new DirectoryNotFoundException(configurationPath);}

            // Check whether mapping configuration file exists
            var fileName = Path.Combine(configurationPath, package.MappingReference + ".xml");
            if (!File.Exists(fileName)) { throw new FileNotFoundException(fileName);}

            // Deserialize the file
            var serializer = new XmlSerializer(typeof(Mapping));
            var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            try
            {
                return (Mapping) serializer.Deserialize(fs);
            }
            finally
            {
                fs.Close();
                fs.Dispose();
            }
        }
    }
}