﻿using System;
using System.Collections.Generic;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public class DataObjectComparator : IComparer<DataObject>
    {
        private readonly string _attributeToCompare;

        public DataObjectComparator(string attributeToCompare)
        {
            _attributeToCompare = attributeToCompare;
        }

        public int Compare(DataObject x, DataObject y)
        {
            return string.Compare(x.GetValueAsString(_attributeToCompare), y.GetValueAsString(_attributeToCompare), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}