''' <summary>
''' Type of database provider.
''' </summary>
''' <remarks>
''' </remarks>
Public Enum DatabaseType As Integer
    Unknown = 0
    Oracle = 2
    MsSql = 4
End Enum
