﻿using System.Collections.Generic;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public class TypeInstructionComparator : IComparer<TypeInstruction>
    {
        public int Compare(TypeInstruction x, TypeInstruction y)
        {
            if (x.Sequence == y.Sequence)
            {
                return 0;
            } else if (x.Sequence < y.Sequence)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}