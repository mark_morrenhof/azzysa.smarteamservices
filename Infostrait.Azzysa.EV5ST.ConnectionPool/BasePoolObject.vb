Imports System.Configuration.ConfigurationSettings
Imports System.Threading

''' <summary>
''' Abstract base class for a connection Object from the Generic Connection Pool
''' </summary>
''' <remarks>
''' 29-08-2006: introduce of Library object, direct usage of backend API depricated.
''' 19-12-2007: introduce pool object base class.
''' 15-05-2009: redesign, sessionless pool.
''' </remarks>
''' <history>
''' 	[t.v.oost]	19-12-2007	Created
''' </history>
Public MustInherit Class BasePoolObject
    Inherits EternalMarshalByRefObject

    Private _baseConnectionString As Dictionary(Of String, String)
    Private _lastUsed As DateTime
    Private _created As DateTime
    Private _hashCode As Integer
    Private _cache As Dictionary(Of String, Object)

    Private Shared _hashCodeCounter As Integer = 0

    ''' <summary>
    ''' The session object, delivered by the pool object.
    ''' </summary>
    Public MustOverride ReadOnly Property Session() As Object

    ''' <summary>
    ''' Possibility to retrieve information about the pool object.
    ''' </summary>
    Public MustOverride ReadOnly Property Information() As Dictionary(Of String, Object)

    Public MustOverride Function Initiate() As Boolean
    Public MustOverride Function Validate(mode As ValidationMode) As Boolean
    Public MustOverride Sub Terminate()
    Public MustOverride Sub TerminateShared()
    Public MustOverride Sub Assigned([assembly] As Reflection.Assembly)

    ''' <summary>
    ''' A pool object level cache.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Cache() As Dictionary(Of String, Object)
        Get
            Return Me._cache
        End Get
    End Property

    Public Overrides Function GetHashCode() As Integer
        Return Me._hashCode
    End Function

    Public ReadOnly Property BaseConnectionString() As Dictionary(Of String, String)
        Get
            Return Me._baseConnectionString
        End Get
    End Property

    ''' <summary>
    ''' Object is available (1) or not (0).
    ''' Not Available means: the object is locked and already in use.
    ''' Available means: the object can be used and is not locked.
    ''' </summary>
    ''' <history>
    ''' 	[t.v.oost]	19-9-2007	Created
    ''' </history>
    Friend Available As Int32

    Public WriteOnly Property IsAvailable() As Boolean
        Set(value As Boolean)
            If value Then
                Interlocked.Exchange(Me.Available, 1)
            Else
                Interlocked.Exchange(Me.Available, 0)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns when this object was claimed and used for the last time.
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    Public ReadOnly Property LastUsed() As DateTime
        Get
            Return Me._lastUsed
        End Get
    End Property

    ''' <summary>
    ''' Returns when this object was created.
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    Public ReadOnly Property Created() As DateTime
        Get
            Return Me._created
        End Get
    End Property

    Public Sub New(connectionString As Dictionary(Of String, String))
        Me.IsAvailable = False
        Me._hashCode = Interlocked.Increment(_hashCodeCounter)

        _lastUsed = Now
        _created = Now
        _baseConnectionString = connectionString
        _cache = New Dictionary(Of String, Object)
    End Sub

    ''' <summary>
    ''' Clears the whole pool object cache.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearCache()
        _cache = New Dictionary(Of String, Object)
    End Sub

    ''' <summary>
    ''' Verifies if this object matches exactly with the properties given
    ''' in the connectionString. Uses caching to perform the match, use ExactMatch to know for sure.
    ''' </summary>
    ''' <param name="connectionString">
    ''' 	The connectionString to match.
    ''' </param>
    ''' <remarks>
    ''' </remarks>
    Public Overridable Function QuickMatch(connectionString As Dictionary(Of String, String)) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Verifies if this object matches exactly with the properties given
    ''' in the connectionString.
    ''' </summary>
    ''' <param name="connectionString">
    ''' 	The connectionString to match.
    ''' </param>
    ''' <remarks>
    ''' </remarks>
    Public Overridable Function ExactMatch(connectionString As Dictionary(Of String, String)) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Verifies if this object matches with the properties given. If the object doesn't match
    ''' exactly, the object tries to modify it's state according to the given connectionString.
    ''' </summary>
    ''' <param name="connectionString">
    ''' 	The connectionString to match.
    ''' </param>
    ''' <remarks>
    ''' </remarks>
    Public Overridable Function TryMatch(connectionString As Dictionary(Of String, String)) As Boolean
        Return True
    End Function

    Public Overridable Sub SetLastUsed()
        Me._lastUsed = Now
    End Sub
End Class