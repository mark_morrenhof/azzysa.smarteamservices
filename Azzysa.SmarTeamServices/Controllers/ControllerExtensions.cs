﻿namespace Azzysa.SmarTeamServices.Controllers
{
    public static class ControllerExtensions
    {

        /// <summary>
        /// Extension method for IExtendableController controllers to return the SmarTeam System Connection String
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public static string GetSystemConnectionString(this IExtendableController controller)
        {
            return System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }

    }
}