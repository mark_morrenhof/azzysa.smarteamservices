﻿using System.Collections.Generic;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public class ParameterComparator : IComparer<Parameter>
    {
        public int Compare(Parameter x, Parameter y)
        {
            if (x.Sequence == y.Sequence)
            {
                return 0;
            }
            else if (x.Sequence < y.Sequence)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}
