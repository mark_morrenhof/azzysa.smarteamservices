﻿using System;
using System.IO;
using System.Windows.Forms;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam.TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var filename = "C:\\Temp\\VI\\error_package.xml";
                var fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(DataPackage));

                var package = (DataPackage) ser.Deserialize(fs);
                fs.Dispose();


                var mappingSortAttribute = "SmarTeamCreationDateSort[TN_DOCUMENTATION.TDM_ORIGIN_ID;TN_DOCUMENTATION.CREATION_DATE;TN_DOCUMENTATION.REVISION;TN_DOCUMENTATION.PAR_REVISION]";

                var s = mappingSortAttribute.Substring(mappingSortAttribute.IndexOf("[", StringComparison.InvariantCultureIgnoreCase) + 1);
                s = s.Substring(0, s.Length - 1);
                var attrs = s.Split(new char[] {Convert.ToChar(";")}, StringSplitOptions.RemoveEmptyEntries);
                
                var originAttributeName = attrs[0];
                var creationDateAttributeName = attrs[1];
                var revisionAttributeName = attrs[2];
                var parentRevisionAttributeName = attrs[3];

                //var sortedObjects = package.DataObjects.GetObjectsSmarTeamRevisionSorted(originAttributeName: "TN_DOCUMENTATION.TDM_ORIGIN_ID", 
                //    revisionAttributeName: "TN_DOCUMENTATION.REVISION",
                //    parentRevisionAttributeName: "TN_DOCUMENTATION.PAR_REVISION");
                var sortedObjects = package.DataObjects.GetObjectsSortedOnCreationDate(originAttributeName, creationDateAttributeName, revisionAttributeName, parentRevisionAttributeName);

                var sortedCount = sortedObjects.Count;
                var rawCount = package.DataObjects.Count();


                MessageBox.Show("Done");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                MessageBox.Show("Done");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
