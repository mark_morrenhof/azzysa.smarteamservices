Imports SmarTeam.Std.Interop.SmarTeam.SmApplic
Imports SmarTeam.Std.Interop.SmarTeam.SmarTeam
Imports SmarTeam.Std.SessionManagement.Service

Imports System.Security
Imports System.Threading
Imports System.Runtime.Remoting

''' <summary>
''' Pooled object, living in the pool. Contains a connection with the SmarTeam API
''' and specific libraries to perform actions in SmarTeam.
'''
''' Connects immediately with the internal SmarTeam API.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[t.v.oost]	12-22-2008	Created
''' </history>
Public MustInherit Class SmarTeamBasePoolObject
    Inherits BasePoolObject

    Private Shared _applicationName As String = "Generic Connection Pool"

    Protected Shared ENGINE_LICENSE_NAME As String = "SmartWEB Server"
    Protected Shared SESSION_LICENSE_NAME As String = "SmartWEB Client"

    ' SmarTeam objects
    Private _engine As SmFreeThreadedEngine

    Private _smSession As ISmSession
    Private _dbID As String
    Private _usID As String
    Private _behavior As ISmBehavior

    Private _staContext As StaThreadSynchronizer.StaSynchronizationContext
    Private _staThread As Threading.Thread

    Private _cachedUserName As String
    Private _cachedUserPassword As String
    Private _cachedReplicaIdentifier As String

    Private _sessionCreated As DateTime
    Private _sessionChanged As DateTime

    ''' <summary>
    ''' Returns information about the object.
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	19-9-2007	Created
    '''  	[t.v.oost]	15-5-2009	Modified
    ''' </history>
    Public Overrides ReadOnly Property Information() As Dictionary(Of String, Object)
        Get
            Dim result = New Dictionary(Of String, Object)

            Try
                result.Add("HashCode", Me.GetHashCode())
                result.Add("CachedUserName", Me._cachedUserName)
                result.Add("ThreadName", _staThread.Name)

                If _smSession Is Nothing Then
                    result.Add("HasSession", False)
                Else
                    result.Add("HasSession", True)

                    If Not _sessionCreated = Nothing Then
                        result.Add("SessionCreated", _sessionCreated)
                    End If

                    If Not _sessionChanged = Nothing Then
                        result.Add("SessionChanged", _sessionChanged)
                    End If

                    If Interlocked.CompareExchange(Me.Available, 0, 1) = 1 Then
                        Try
                            result.Add("UserLoggedOn", _smSession.UserLoggedOn)
                            result.Add("UserLogin", _smSession.UserMetaInfo.UserLogin)
                            result.Add("UserAdmin", _smSession.UserMetaInfo.UserAdmin)
                            result.Add("UserId", _smSession.UserMetaInfo.UserId)
                            result.Add("DatabaseName", _smSession.MetaInfo.Database.Name)
                            result.Add("DatabaseVersion", _smSession.MetaInfo.Database.Version)
                        Catch ex As Exception
                            Throw
                        Finally
                            ' Set available to true, in an atomic operation
                            Me.IsAvailable = True
                        End Try
                    End If
                End If
            Catch ex As Exception
                result.Add("Error", ex.Message)
            End Try

            Return result
        End Get
    End Property

    Protected MustOverride ReadOnly Property Mode() As SmarTeamPoolObjectMode
    Protected Enum SmarTeamPoolObjectMode
        Navigator = 0
        Editor = 1
        Client = 2
        WebEditor = 3
    End Enum

    Public Sub New(connectionString As Dictionary(Of String, String))
        MyBase.New(connectionString)
    End Sub

    Private Function GetStaContext() As StaThreadSynchronizer.StaSynchronizationContext
        If _staContext Is Nothing Then
            _staContext = New StaThreadSynchronizer.StaSynchronizationContext
        End If

        Return _staContext
    End Function

    Public Overrides Function Initiate() As Boolean
        Dim userName As String = String.Empty, userPassword As String = String.Empty
        If Me.BaseConnectionString.ContainsKey("UserName") Then
            userName = Me.BaseConnectionString("UserName").ToString
        End If
        If Me.BaseConnectionString.ContainsKey("UserPassword") Then
            userPassword = Me.BaseConnectionString("UserPassword").ToString
        End If

        Dim params As New StaParams
        params.UserName = userName
        params.UserPassword = userPassword

        Me.GetStaContext().Send(New StaThreadSynchronizer.SendOrPostCallback(AddressOf InitiateSta), params)

        Return params.Result
    End Function

    ''' <summary>
    ''' Connect to SmarTeam, initiate engine and session.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	19-9-2007	Created
    ''' </history>
    Public Sub InitiateSta(state As Object)
        Dim params As StaParams = DirectCast(state, StaParams)

        Try
            _staThread = Threading.Thread.CurrentThread

            If Me.Mode = SmarTeamPoolObjectMode.Client Then
                ' Client mode, get session and engine from a running SmarTeam
                ' Get active application and engine
                Dim application As SmApplication, engine As ISmEngine
                application = DirectCast(GetObject(, "smarteam.application"), SmApplication)
                engine = DirectCast(application.Engine, SmFreeThreadedEngine)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(application)

                ' Get session
                MyBase.ClearCache()
                Me._smSession = engine.Sessions(0)
            Else
                ' Determine license
                If Me.Mode = SmarTeamPoolObjectMode.Editor Then
                    SmarTeamBasePoolObject.SESSION_LICENSE_NAME = "SmartWEB Client Read/Write"
                ElseIf Me.Mode = SmarTeamPoolObjectMode.WebEditor Then
                    SmarTeamBasePoolObject.SESSION_LICENSE_NAME = "SmartWEB Client (Read/Write)"
                End If

                ' Normal mode, create new engine (or use existing one) and session
                ' Create engine, if not exists
                Me.GetEngine()

                ' Read database identifier
                If Me.BaseConnectionString.ContainsKey("DatabaseId") Then
                    If Not Me.BaseConnectionString("DatabaseId") Is Nothing Then
                        _dbID = Me.BaseConnectionString("DatabaseId").ToString
                    End If
                End If
                If _dbID Is Nothing Or _dbID = String.Empty Then
                    Throw New Exception("The parameter 'DatabaseId' does not exist in the connectionstring.")
                End If

                ' Now create a user session
                Me.GetStaContext().Send(New StaThreadSynchronizer.SendOrPostCallback(AddressOf CreateSessionSta), params)
            End If
        Catch ex As Exception
            Me.Terminate()
        End Try
    End Sub

    Private Function GetEngine() As SmFreeThreadedEngine
        If _engine Is Nothing Then
            _engine = New SmFreeThreadedEngine
            DirectCast(_engine, ISmRawEngine).LicenseName = ENGINE_LICENSE_NAME

            _engine.ServerMode = True
            _engine.Init("SmTeam32.Ini")

        End If

        Return _engine
    End Function

    ''' <summary>
    ''' The corresponding SmarTeam session.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	1-30-2009	Created
    ''' </history>
    Public Overrides ReadOnly Property Session() As Object
        Get
            Return Me._smSession
        End Get
    End Property

    Public Function GetBehavior(checkAuthorization As Boolean) As ISmBehavior
        If _behavior Is Nothing Then
            ' Create new behavior
            _behavior = Me._smSession.ObjectStore.DefaultBehavior.Clone
            _behavior.InvokeScripts = False
            _behavior.CheckAttachedFileChanged = ConfirmOperationEnum.coYesToAll
            _behavior.CheckLinksOnDelete = CheckLinksOnDeleteEnum.cldDelete
            _behavior.ConfirmAttachedFileDeletion = ConfirmOperationEnum.coYesToAll
            _behavior.ConfirmOperations = ConfirmOperationEnum.coYesToAll
        End If

        _behavior.CheckAuthorization = checkAuthorization

        If Not checkAuthorization Then
            _behavior.ViewObjectAuthorization = ViewObjectAuthorizationEnum.voaNotToCheck
        Else
            _behavior.ViewObjectAuthorization = ViewObjectAuthorizationEnum.voaCheckAndDeleteFromList
        End If

        Return _behavior
    End Function

#Region " Session validation "
    Public Overrides Function Validate(mode As ValidationMode) As Boolean
        If Me.Mode = SmarTeamPoolObjectMode.Client Then
            Return True
        End If

        ' Check if we have a valid SmarTeam session, if not try to make it valid
        If Me.ValidateSmSession(mode) Then
            Return True
        Else

            ' Try to create a new session
            Dim userName As String = String.Empty, userPassword As String = String.Empty
            If Me.BaseConnectionString.ContainsKey("UserName") Then
                userName = Me.BaseConnectionString("UserName").ToString
            End If
            If Me.BaseConnectionString.ContainsKey("UserPassword") Then
                userPassword = Me.BaseConnectionString("UserPassword").ToString
            End If

            Dim params As New StaParams
            params.UserName = userName
            params.UserPassword = userPassword

            Me.GetStaContext().Send(New StaThreadSynchronizer.SendOrPostCallback(AddressOf CreateSessionSta), params)

            Return params.Result
        End If
    End Function

    Private Function ValidateSmSession(mode As ValidationMode) As Boolean
        Try
            ' 0) Do we have a session?
            If Me._smSession Is Nothing Then
                Throw New Exception("Session is null.")
            End If

            ' 1) Is a user logged on to this session?
            If Me._smSession.UserLoggedOn = False Then
                Throw New Exception("No user logged on.")
            End If

            ' 2) Perform advanced validation if requested
            If mode = ValidationMode.Full Then
                ' A) Does it have a valid database connection?
                Try
                    Dim q As ISmSimpleQuery
                    q = Me._smSession.ObjectStore.NewSimpleQuery()
                    q.RunQueryMode = RunQueryModeEnum.rqmDirect
                    q.SelectStatement = "SELECT SMARTEAM_VERSION FROM TDM_DB_VERSION"
                    q.Run()
                Catch ex As Exception
                    Throw New Exception("No database connection.", ex)
                End Try

                ' B) Can we connect with the session management service/is the session valid in the session management service?
                If Not IsSessionValidInSessionManagement(Me._smSession.USID) Then
                    Throw New Exception("Session is not valid (anymore) in session management service.")
                End If
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region " Matching a pool object "
    Public Overrides Function QuickMatch(connectionString As Dictionary(Of String, String)) As Boolean
        If Me.Mode = SmarTeamPoolObjectMode.Client Then
            Return True
        End If

        ' Perform quick match
        If (Me._cachedUserName.ToLower() = connectionString("UserName").ToString.ToLower() Or connectionString("UserName").ToString = String.Empty) Then
            Return True
        End If

        Return False
    End Function

    Public Overrides Function ExactMatch(connectionString As Dictionary(Of String, String)) As Boolean
        If Me.Mode = SmarTeamPoolObjectMode.Client Then
            Return True
        End If

        Try
            ' Check if all information is available
            If connectionString.ContainsKey("DatabaseId") And _
                connectionString.ContainsKey("UserName") _
                Then

                ' Perform exact match
                If Not Me._smSession Is Nothing Then
                    If Me._smSession.Database.ReplicaIdentifier.ToLower() = connectionString("DatabaseId").ToString.ToLower() And _
                        (Me._smSession.UserMetaInfo.UserLogin.ToLower() = connectionString("UserName").ToString.ToLower() Or connectionString("UserName").ToString = String.Empty) Then

                        Return True
                    End If
                End If
            End If

            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Overrides Function TryMatch(connectionString As Dictionary(Of String, String)) As Boolean
        If Me.Mode = SmarTeamPoolObjectMode.Client Then
            Return True
        End If

        Try
            If Me.ExactMatch(connectionString) Then
                Return True
            Else
                ' Check if all information is available
                If connectionString.ContainsKey("DatabaseId") And _
                    connectionString.ContainsKey("UserName") _
                    Then

                    ' Perform try match
                    If Not Me._smSession Is Nothing Then
                        If Me._smSession.Database.ReplicaIdentifier.ToLower() = connectionString("DatabaseId").ToString.ToLower() Then
                            Dim userName, password As String
                            userName = Me.GetUserName(connectionString("UserName").ToString)
                            password = Me.GetUserPassword(connectionString("UserName").ToString)

                            If Not password Is Nothing Then
                                Dim params As New StaParams
                                params.UserName = userName
                                params.UserPassword = password

                                Me.GetStaContext().Send(New StaThreadSynchronizer.SendOrPostCallback(AddressOf ChangeSessionSta), params)

                                Return params.Result
                            Else
                                If userName Is Nothing Or userName = String.Empty Then
                                    Throw New SecurityException(String.Format("Unknown user account.", userName))
                                Else
                                    Throw New SecurityException(String.Format("User '{0}' does not exist/password retrieval failed.", userName))
                                End If
                            End If
                        Else
                            Throw New Exception(String.Format("Database identifier mismatch. Connected database: '{0}'.", Me._smSession.Database.ReplicaIdentifier))
                        End If
                    End If
                End If

                Return False
            End If
        Catch ex As SecurityException
            Throw
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region " Session Management Service "
    Private Function IsSessionValidInSessionManagement(ByVal usid As String) As Boolean
        Dim flagArray As Boolean() = GetSessionManagement.IsUserSessionsValid(New String() {usid})
        Return (((Not flagArray Is Nothing) AndAlso (flagArray.Length > 0)) AndAlso flagArray(0))
    End Function

    Private Function GetSessionManagement() As IFuss
        If Me.BaseConnectionString.ContainsKey("SessionManagementUri") Then
            Dim uri = Me.BaseConnectionString("SessionManagementUri")
            Return TryCast(RemotingServices.Connect(GetType(Fuss), uri), IFuss)
        Else
            Throw New Exception("The parameter 'SessionManagementUri' does not exist in the connectionstring.")
        End If
    End Function
#End Region

    Private Class StaParams
        Public UserName As String
        Public UserPassword As String
        Public Result As Boolean
    End Class

    Private Shared _createSessionLock As New Object

    Private Sub ChangeSessionSta(state As Object)
        Me.CreateSessionSta(state)
    End Sub

    Private Sub CreateSessionSta(state As Object)
        Dim params As StaParams = DirectCast(state, StaParams)

        Try
            Me.TryReleaseSession(ReleaseReason.[New])

            ' Set authentication data
            Dim eng = Me.GetEngine()

            ' Get session
            SyncLock _createSessionLock
                MyBase.ClearCache()
                _smSession = New SmSession
                TryCast(_smSession, ISmRawSession).LicenseName = SESSION_LICENSE_NAME

                _smSession.Init(eng, _applicationName, "SmTeam32.Ini")
                TryCast(_smSession, ISmRawSession).SessionManagementKeepAlive = False

                Dim database As SmDatabase = Nothing
                database = _smSession.Engine.FindDatabaseByReplicaIdentifier(_dbID)

                _smSession.OpenDatabaseConnection(database.Alias, database.Password, True)
                _smSession.UserLogin(params.UserName, params.UserPassword)
            End SyncLock

            If _smSession Is Nothing Then
                Throw New NullReferenceException
            End If

             _sessionCreated = Now

            ' Check if we have a valid session
            If Me.ValidateSmSession(ValidationMode.Quick) Then
                _cachedUserName = params.UserName
                _cachedUserPassword = params.UserPassword
                _cachedReplicaIdentifier = _smSession.Database.ReplicaIdentifier

                params.Result = True
            Else
                Throw New Exception("Session is not valid.")
            End If
        Catch ex As Exception
            Me.TryReleaseSession(ReleaseReason.Error)

            Dim log = New EventLog("Azzysa")
            log.Source = "Azzysa"
            log.WriteEntry(ex.ToString(), EventLogEntryType.Error)

            params.Result = False
        End Try
    End Sub

    Private Enum ReleaseReason As Integer
        [New] = 0
        [Error] = 1
        [Terminate] = 2
    End Enum

    Private Sub TryReleaseSession(reason As ReleaseReason)
        Try
            ' If we have a session already, clear/release the session.
            If Not Me.Mode = SmarTeamPoolObjectMode.Client Then
                If Not _smSession Is Nothing Then
                    _smSession.Close()
                    _behavior = Nothing
                    _smSession = Nothing
                    MyBase.ClearCache()
                    GC.Collect()
                End If
            End If
        Finally
            _cachedUserName = String.Empty
            _cachedUserPassword = String.Empty
            _cachedReplicaIdentifier = String.Empty
        End Try
    End Sub

    Public Function GetUserPassword(userName As String) As String
        Dim smQuery As ISmQuery = Nothing, smQueryDefinition As ISmQueryDefinition
        Try
            smQuery = Me._smSession.ObjectStore.NewQuery
            smQueryDefinition = smQuery.QueryDefinition()
            smQueryDefinition.CaseSensitive = False
            smQueryDefinition.Roles.Add(Me._smSession.UserMetaInfo.User.ClassId, "F")
            smQueryDefinition.Select.Add("LOGIN", "F", False)
            smQueryDefinition.Select.Add("USER_PASSWORD", "F", False)
            smQueryDefinition.Where.Add("", "LOGIN", "=", userName, False, "F")
            smQueryDefinition.Where.Add("", "STATE", "!=", 4, False, "F")
            smQuery.QueryDefinition.RecordLimit = 1
            smQuery.RunEx(Me.GetBehavior(False))

            If smQuery.QueryResult.RecordCount > 0 Then
                If smQuery.QueryResult.GetRecord(0).ValueAsString("USER_PASSWORD") Is Nothing Then
                    Return ""
                Else
                    Return smQuery.QueryResult.GetRecord(0).ValueAsString("USER_PASSWORD").ToString
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Get the user name/login stored in SmarTeam so we do not have problems with casing.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	13-10-2010	Created
    ''' </history>
    Private Function GetUserName(userName As String) As String
        Dim smQuery As ISmQuery = Nothing, smQueryDefinition As ISmQueryDefinition
        Try
            smQuery = Me._smSession.ObjectStore.NewQuery
            smQueryDefinition = smQuery.QueryDefinition()
            smQueryDefinition.CaseSensitive = False
            smQueryDefinition.Roles.Add(Me._smSession.UserMetaInfo.User.ClassId, "F")
            smQueryDefinition.Select.Add("LOGIN", "F", False)
            smQueryDefinition.Where.Add("", "LOGIN", "=", userName, False, "F")
            smQueryDefinition.Where.Add("", "STATE", "!=", 4, False, "F")
            smQuery.QueryDefinition.RecordLimit = 1
            smQuery.RunEx(Me.GetBehavior(False))

            If smQuery.QueryResult.RecordCount > 0 Then
                If smQuery.QueryResult.GetRecord(0).ValueAsString("LOGIN") Is Nothing Then
                    Return ""
                Else
                    Return smQuery.QueryResult.GetRecord(0).ValueAsString("LOGIN").ToString
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Release SmarTeam session.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	19-9-2007	Created
    ''' </history>
    Public Overrides Sub Terminate()
        Me.TryReleaseSession(ReleaseReason.Terminate)
    End Sub

    ''' <summary>
    ''' Terminate SmarTeam engine.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	19-9-2007	Created
    ''' </history>
    Public Overrides Sub TerminateShared()
        Me.GetEngine.Terminate()
    End Sub

    ''' <summary>
    ''' Triggers when an pool object is assigned.
    ''' </summary>
    ''' <param name="assembly"></param>
    ''' <remarks></remarks>
    Public Overrides Sub Assigned([assembly] As Reflection.Assembly)
    End Sub
End Class