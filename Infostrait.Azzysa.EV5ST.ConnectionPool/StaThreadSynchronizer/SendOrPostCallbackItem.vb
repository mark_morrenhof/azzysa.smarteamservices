Imports System.Text
Imports System.Threading

Namespace StaThreadSynchronizer
    Public Delegate Sub SendOrPostCallback(state As Object)

    Friend Enum ExecutionType
        Post
        Send
    End Enum

    Friend Class SendOrPostCallbackItem
        Private mState As Object
        Private mExeType As ExecutionType
        Private mMethod As SendOrPostCallback
        Private mAsyncWaitHandle As New ManualResetEvent(False)
        Private mException As Exception = Nothing

        Friend Sub New(callback As SendOrPostCallback, state As Object, type As ExecutionType)
            mMethod = callback
            mState = state
            mExeType = type
        End Sub

        Friend ReadOnly Property Exception() As Exception
            Get
                Return mException
            End Get
        End Property

        Friend ReadOnly Property ExecutedWithException() As Boolean
            Get
                Return (Not mException Is Nothing)
            End Get
        End Property

        ' this code must run ont the STA thread
        Friend Sub Execute()
            If mExeType = ExecutionType.Send Then
                Send()
            Else
                Post()
            End If
        End Sub

        ' calling thread will block until mAsyncWaitHanel is set
        Friend Sub Send()
            Try
                ' call the thread
                mMethod(mState)
            Catch e As Exception
                mException = e
            Finally
                mAsyncWaitHandle.[Set]()
            End Try
        End Sub

        ''' <summary>
        ''' Unhandle execptions will terminate the STA thread
        ''' </summary>
        Friend Sub Post()
            mMethod(mState)
        End Sub

        Friend ReadOnly Property ExecutionCompleteWaitHandle() As WaitHandle
            Get
                Return mAsyncWaitHandle
            End Get
        End Property
    End Class
End Namespace
