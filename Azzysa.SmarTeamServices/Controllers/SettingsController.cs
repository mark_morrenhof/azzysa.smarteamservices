﻿using System.Web.Http;

namespace Azzysa.SmarTeamServices.Controllers
{

    /// <summary>
    /// API controller that serves setting functinonality based on SmarTeam
    /// </summary>
    public class SettingsController : ApiController, 
        IExtendableController
    {

        /// <summary>
        /// Retrieve the appropriate settings based on site and server name
        /// </summary>
        /// <param name="siteName">Name of the site where the calling client is a member of</param>
        /// <param name="serverName">Name of the calling server</param>
        /// <returns></returns>
        public Infostrait.Azzysa.Providers.SettingsDictionary Retrieve(string siteName, string serverName)
        {
            using (var connection = new Infostrait.Azzysa.EV5ST.ConnectionPool.Connections.EV5STConnection(this.GetSystemConnectionString()))
            {
                // Open the connection with SmarTeam
                connection.Open();

                // Get and check SmarTeamServices config directory (relative to script directory)
                var configDirectory = System.IO.Path.Combine(connection.Session.Config.get_Value("$Admin\\Directory Structure\\ScriptDirectory"), "SmarTeamServices");
                if (System.IO.Directory.Exists(configDirectory))
                {
                    // Get global configuration file (containing all default-platform generic) settings
                    var serializer = new System.Runtime.Serialization.DataContractSerializer(typeof(Infostrait.Azzysa.Providers.SettingsDictionary));
                    var fs = new System.IO.FileStream(System.IO.Path.Combine(configDirectory, "azzysa.settings.xml"), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    var generalSettings = (Infostrait.Azzysa.Providers.SettingsDictionary)serializer.ReadObject(fs);
                    fs.Dispose();

                    // Check for site specific settings
                    if (System.IO.File.Exists(System.IO.Path.Combine(configDirectory, string.Format("azzysa.settings.{0}.xml", siteName))))
                    {
                        // Get site specific settings
                        fs = new System.IO.FileStream(System.IO.Path.Combine(configDirectory, string.Format("azzysa.settings.{0}.xml", siteName)), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        var settings = (Infostrait.Azzysa.Providers.SettingsDictionary)serializer.ReadObject(fs);
                        fs.Dispose();

                        // Merge with general settings
                        foreach (var setting in settings)
                        {
                            if (generalSettings.ContainsKey(setting.Key))
                            {
                                // Update setting in general setting collection
                                generalSettings[setting.Key] = setting.Value;
                            }
                            else
                            {
                                // Add settings, since it is not in the general collection
                                generalSettings.Add(setting.Key, setting.Value);
                            }
                        }
                    }

                    // Check for server specific settings
                    if (System.IO.File.Exists(System.IO.Path.Combine(configDirectory, string.Format("azzysa.settings.{0}.{1}.xml", siteName, serverName))))
                    {
                        // Get server specific settings
                        fs = new System.IO.FileStream(System.IO.Path.Combine(configDirectory, string.Format("azzysa.settings.{0}.{1}.xml", siteName, serverName)), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        var settings = (Infostrait.Azzysa.Providers.SettingsDictionary)serializer.ReadObject(fs);
                        fs.Dispose();

                        // Merge with general settings
                        foreach (var setting in settings)
                        {
                            if (generalSettings.ContainsKey(setting.Key))
                            {
                                // Update setting in general setting collection
                                generalSettings[setting.Key] = setting.Value;
                            }
                            else
                            {
                                // Add settings, since it is not in the general collection
                                generalSettings.Add(setting.Key, setting.Value);
                            }
                        }
                    }

                    // Finished
                    return generalSettings;
                }
                else
                {
                    // Does not exists
                    throw new System.IO.DirectoryNotFoundException(configDirectory);
                }

            }
        }
    }
}