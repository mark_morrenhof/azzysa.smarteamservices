Imports SmarTeam.Std.Interop.SmarTeam.SmApplic

''' <summary>
''' Use this class to retrieve a SmarTeam API session.
''' Design and behavior is similar to the System.Data.SqlClient.SqlConnection class.
''' </summary>
''' <remarks>
''' </remarks>
Public Class EV5STConnection
    Implements IDisposable

    Private _assignedPoolObject As SmarTeamBasePoolObject
    Private _connectionString As Dictionary(Of String, String)

    ''' <summary>
    ''' Returns the SmarTeam session object.
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' Call .Open() before using this property, otherwise this property will give
    ''' a NullReferenceException.
    ''' </remarks>
    Public ReadOnly Property Session() As ISmSession
        Get
            Return DirectCast(_assignedPoolObject.Session, ISmSession)
        End Get
    End Property

    ''' <summary>
    ''' Returns cache object that belongs to the active SmarTeam session.
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    Public ReadOnly Property Cache() As Dictionary(Of String, Object)
        Get
            Return DirectCast(_assignedPoolObject.Cache, Dictionary(Of String, Object))
        End Get
    End Property

    ''' <summary>
    ''' Returns the current connection string.
    ''' Note: the connection string is converted to a dictionary.
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    Public ReadOnly Property ConnectionString() As Dictionary(Of String, String)
        Get
            Return _connectionString
        End Get
    End Property

    ''' <summary>
    ''' Creates a new SmarTeam connection.
    ''' Authentication of SmarTeam users is not needed; you can implement authentication 
    ''' using SmarTeam (use EV5STConnection.Authenticate(userName, password)) or using 
    ''' your own authentication mechanism.
    ''' Connection string. Examples: DatabaseId=04J484GJFHS4718346134GJF;UserName=joe
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub New(connectionString As String)
        _connectionString = ConnectionStringBuilder.ToDictionary(connectionString)
    End Sub

    Public Sub Open()
        ' Get a valid pool object from the connection pool.
        _assignedPoolObject = DirectCast(PoolManager.Instance.ReserveObject(_connectionString), SmarTeamBasePoolObject)
        _assignedPoolObject.Assigned(Reflection.Assembly.GetCallingAssembly())
    End Sub

    ''' <summary>
    ''' Validates the assigned connection/session/poolobject.
    ''' Performs a full validation (session object exists, user logged on, check database connection, check session management service connection)
    ''' Tries to create a new connection when validation fails.
    ''' This operation can take time!
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub Validate()
        _assignedPoolObject.Validate(ValidationMode.Full)
    End Sub

    ''' <summary>
    ''' Returns a best-practice server side SmarTeam behavior.
    ''' This behavior doesn't run scripts.
    ''' </summary>
    ''' <param name="checkAuthorization">
    ''' 	Check authorization or not.
    ''' 	Value Type: <see cref="Boolean" />	(System.Boolean)
    ''' </param>
    ''' <remarks>
    ''' </remarks>
    Public Function GetBehavior(checkAuthorization As Boolean) As ISmBehavior
        Return _assignedPoolObject.GetBehavior(checkAuthorization)
    End Function

    ''' <summary>
    ''' Returns a best-practice server side SmarTeam behavior.
    ''' This behavior doesn't run scripts.
    ''' This behavior checks authorization.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Public Function GetBehavior() As ISmBehavior
        Return _assignedPoolObject.GetBehavior(True)
    End Function

    ''' <summary>
    ''' Performs an authentication check, using the SmarTeam API. Returns if the user can logon with the specified password.
    ''' Note: this function does not affect the user logged on in the current SmarTeam session.
    ''' </summary>
    ''' <param name="userName">
    ''' 	The user login name.
    ''' 	Value Type: <see cref="String" />	(System.String)
    ''' </param>
    ''' <param name="password">
    ''' 	The user's password.
    ''' 	Value Type: <see cref="String" />	(System.String)
    ''' </param>
    ''' <remarks>
    ''' </remarks>
    Public Function CheckAuthentication(userName As String, password As String) As Boolean
        Dim upwd = _assignedPoolObject.GetUserPassword(userName)

        If password = upwd And upwd IsNot Nothing Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Performs an authentication check, using the SmarTeam API. Returns if the user can logon.
    ''' Note: this function does not affect the user logged on in the current SmarTeam session.
    ''' </summary>
    ''' <param name="userName">
    ''' 	[description goes here]. 
    ''' 	Value Type: <see cref="String" />	(System.String)
    ''' </param>
    ''' <exception cref="System.ApplicationException">
    ''' 	Thrown when... 
    ''' </exception>
    ''' <returns><see cref="Boolean" />	(System.Boolean)</returns>
    ''' <remarks>
    ''' </remarks>
    Public Function CheckAuthentication(userName As String) As Boolean
        If _assignedPoolObject.GetUserPassword(userName) Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub Close()
        Me.Dispose()
    End Sub

    Public Sub Dispose() Implements System.IDisposable.Dispose
        ' Make available for others
        If Not _assignedPoolObject Is Nothing Then
            _assignedPoolObject.IsAvailable = True
        End If
        Me._assignedPoolObject = Nothing
    End Sub
End Class