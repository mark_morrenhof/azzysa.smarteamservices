''' <summary>
''' Pooled object, living in the pool. Contains a connection with the SmarTeam API
''' and specific libraries to perform actions in SmarTeam.
'''
''' Uses one SmarTeam Navigator license.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[t.v.oost]	1-30-2009	Created
''' </history>
Public Class SmarTeamNavigatorPoolObject
    Inherits SmarTeamBasePoolObject

    Protected Overrides ReadOnly Property Mode() As SmarTeamPoolObjectMode
        Get
            Return SmarTeamBasePoolObject.SmarTeamPoolObjectMode.Navigator
        End Get
    End Property

    Public Sub New(connectionString As Dictionary(Of String, String))
        MyBase.New(connectionString)
    End Sub
End Class