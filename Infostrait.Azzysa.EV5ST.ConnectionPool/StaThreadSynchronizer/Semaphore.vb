Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.ComponentModel

Namespace StaThreadSynchronizer
    Public NotInheritable Class Semaphore
        Inherits WaitHandle

        Public Sub New()
            Me.New(1, 1)
        End Sub

        Public Sub New(initialCount As Integer, maximumCount As Integer)
            If initialCount < 0 OrElse initialCount > maximumCount Then
                Throw New ArgumentOutOfRangeException("initialCount")
            End If
            If maximumCount < 1 Then
                Throw New ArgumentOutOfRangeException("maximumCount")
            End If
            Dim h As IntPtr = CreateSemaphore(IntPtr.Zero, initialCount, maximumCount, Nothing)
            If h.Equals(WaitHandle.InvalidHandle) OrElse h.Equals(IntPtr.Zero) Then
                Throw New Win32Exception
            End If
            Handle = h
        End Sub

        Public Sub Release()
            Dim previousCount As Integer
            If Not ReleaseSemaphore(Handle, 1, previousCount) Then
                Throw New Win32Exception
            End If
        End Sub

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function CreateSemaphore(lpSemaphoreAttributes As IntPtr, lInitialCount As Integer, lMaximumCount As Integer, lpName As String) As IntPtr
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function ReleaseSemaphore(hSemaphore As IntPtr, lReleaseCount As Integer, lpPreviousCount As Integer) As Boolean
        End Function
    End Class
End Namespace