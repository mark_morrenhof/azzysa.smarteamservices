Imports System.Runtime.Serialization

''' <summary>
''' This exception is thrown when there is no pool object available in the connection pool.
''' </summary>
''' <remarks>
''' </remarks>
<Serializable()> _
Public Class PoolObjectUnavailableException
    Inherits Exception

    Sub New()
        MyBase.New()
    End Sub

    Sub New(message As String)
        MyBase.New(message)
    End Sub

    Sub New(message As String, inner As Exception)
        MyBase.New(message, inner)
    End Sub

    Protected Sub New(info As SerializationInfo, context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class