''' <summary>
''' Pooled object, living in the pool. Contains a connection with the SmarTeam API
''' and specific libraries to perform actions in SmarTeam.
'''
''' Uses a running SmarTeam Windows client, without additional licenses.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[t.v.oost]	1-30-2009	Created
''' </history>
Public Class SmarTeamClientPoolObject
    Inherits SmarTeamBasePoolObject

    Protected Overrides ReadOnly Property Mode() As SmarTeamPoolObjectMode
        Get
            Return SmarTeamBasePoolObject.SmarTeamPoolObjectMode.Client
        End Get
    End Property

    Public Sub New(connectionString As Dictionary(Of String, String))
        MyBase.New(connectionString)
    End Sub
End Class