﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public static class ObjectCollectionExtensions
    {

        public static List<DataObject> GetObjectsSmarTeamRevisionSorted(this ObjectCollection collection, string originAttributeName,
            string revisionAttributeName, string parentRevisionAttributeName)
        {
            var sortedCollection = new List<DataObject>();

            // Sort the collection to group all revision groups
            var revisionGroupSortedCollection = collection.GetObjectsSorted(originAttributeName);
            var processedRevisionGroups = "";

            // Iterate through all objects sorted by revision group
            var previousRevisionGroup = "";
            foreach (var dataObject in revisionGroupSortedCollection)
            {
                // Get revision group identifier
                string revisionGroup;
                try
                {
                    revisionGroup = dataObject.GetValueAsString(originAttributeName);
                }
                catch (System.InvalidOperationException ex)
                {
                    throw new ApplicationException($"Unable to get revision group identifier because of missing attribute: " + originAttributeName);
                }

                // Get next revision group since in the next while loop we get all iterations from each revision group
                if (previousRevisionGroup != revisionGroup && (processedRevisionGroups.IndexOf(revisionGroup + ";", StringComparison.InvariantCultureIgnoreCase) < 0))
                {
                    // Take root / first revision from this group
                    previousRevisionGroup = revisionGroup;
                    processedRevisionGroups += revisionGroup + ";";

                    // Check for null or empty root
                    var requestedRevision = GetRevision(revisionGroupSortedCollection, revisionAttributeName, originAttributeName, revisionGroup, parentRevisionAttributeName, null, true, true);
                    if (requestedRevision == null)
                    {
                        // Check for non null or empty root
                        requestedRevision = GetRevision(revisionGroupSortedCollection, revisionAttributeName, originAttributeName, revisionGroup, parentRevisionAttributeName, null, true, false);
                    }

                    // Check if the first revision has been found
                    if (requestedRevision == null) { throw new Exception("Invalid number of root revisions (should be 1, actual is 0)"); }
                    sortedCollection.Add(requestedRevision);

                    // Iterate through collection until all revisions have been found
                    while (requestedRevision != null)
                    {
                        // Get next revision
                        requestedRevision = GetRevision(revisionGroupSortedCollection, revisionAttributeName, originAttributeName, revisionGroup, parentRevisionAttributeName,
                                requestedRevision.GetValueAsString(revisionAttributeName), false, false);
                        
                        // Check if revision has been found
                        if (requestedRevision != null)
                        {
                            sortedCollection.Add(requestedRevision);
                        }
                    }
                }
            }
            
            // Check if the number of sorted DataObjects is equal to the number of input objects
            if (sortedCollection.Count != collection.Collection.Collection.Count)
            {
                // Number of sorted revision objects is not according the number of input objects
                throw new Exception("Number of sorted revision objects (" + sortedCollection.Count + ") is not according the number of input objects(" +
                        collection.Collection.Collection.Count + ")");
            }
            else {
                // Number of objects are equal, set the result collection to the package
                collection.Collection.Collection = sortedCollection;
            }

            // Finished, return sorted collection
            return collection.Collection.Collection;
        }

        public static List<DataObject>  GetObjectsSortedOnCreationDate(this ObjectCollection collection,
            string originAttributeName, string creationDateAttributeName, string revisionAttributeName, string parentRevisionAttributeName)
        {
            DataObject requestedRevision;
            var sortedCollection = new List<DataObject>();

            // Sort the collection to group all revision groups
            var revisionGroupSortedCollection = collection.GetObjectsSorted(originAttributeName);
            var processedRevisionGroups = "";

            // Iterate through all objects sorted by revision group
            var previousRevisionGroup = "";
            foreach (var dataObject in revisionGroupSortedCollection)
            {
                // Get revision group identifier
                string revisionGroup;
                try
                {
                    revisionGroup = dataObject.GetValueAsString(originAttributeName);
                }
                catch (System.InvalidOperationException ex)
                {
                    throw new ApplicationException($"Unable to get revision group identifier because of missing attribute: " + originAttributeName);
                }

                // Get next revision group since in the next while loop we get all iterations from each revision group
                if (previousRevisionGroup != revisionGroup && (processedRevisionGroups.IndexOf(revisionGroup + ";", StringComparison.InvariantCultureIgnoreCase) < 0))
                {
                    // Take root / first revision from this group
                    previousRevisionGroup = revisionGroup;
                    processedRevisionGroups += revisionGroup + ";";

                    // Get all revisions from this group
                    var revisions = GetAllRevisionsById(revisionGroupSortedCollection, originAttributeName, revisionGroup);

                    // Sort this collection on creation date
                    revisions.Sort(new CreationDateObjectComparator(creationDateAttributeName));

                    // Correct/change null or empty parent revision objects that are not the root revision
                    // In case of using a nullable revision schema, parent revision of non-root revision may be empty
                    // Therefore set these par-revision values to <EMPTY> so the plugin can recognize these revisions as such
                    var idx = 0;
                    foreach (var revision in revisions)
                    {
                        // Do not change root revision
                        if (idx > 0)
                        {
                            // Get previous revision value
                            var previousRevision =
                                revisions[idx - 1].Attributes.First(
                                    a =>
                                        string.Equals(revisionAttributeName, a.Name,
                                            StringComparison.InvariantCultureIgnoreCase)).Value as string;

                            // Check for null or empty previous revision
                            if (string.IsNullOrWhiteSpace(previousRevision))
                            {
                                // Set current 'parent revision' value to <EMPTY>
                                revision.Attributes.First(
                                    a =>
                                        string.Equals(parentRevisionAttributeName, a.Name,
                                            StringComparison.InvariantCultureIgnoreCase)).Value = "<EMPTY>";
                            }
                        }

                        // Increment index
                        idx++;
                    }

                    // Add all objects to the complete collection
                    sortedCollection.AddRange(revisions);
                }
            }

            // Check if the number of sorted DataObjects is equal to the number of input objects
            if (sortedCollection.Count != collection.Collection.Collection.Count)
            {
                // Number of sorted revision objects is not according the number of input objects
                throw new Exception("Number of sorted revision objects (" + sortedCollection.Count + ") is not according the number of input objects(" +
                        collection.Collection.Collection.Count + ")");
            }
            else {
                // Number of objects are equal, set the result collection to the package
                collection.Collection.Collection = sortedCollection;
            }

            // Finished, return sorted collection
            return collection.Collection.Collection;
        }

        public static List<DataObject> GetObjectsSorted(this ObjectCollection collection, string sortAttribute)
        {
            collection.Collection.Collection.Sort(new DataObjectComparator(sortAttribute));
            return collection.Collection.Collection;
        }

        /// <summary>
        /// Helper method for the SmarTeamCreationDateSort function
        /// </summary>
        /// <param name="collection">Collection to get the revisions from</param>
        /// <param name="revisionIdAttributeName">Attribute name of the revision group identifier</param>
        /// <param name="revisionId">Revision group identifier to retrieve</param>
        /// <returns></returns>
        private static List<DataObject> GetAllRevisionsById(IEnumerable<DataObject> collection, string revisionIdAttributeName, string revisionId)
        {
            var returnValue = new List<DataObject>();
            foreach (var obj in collection)
            {
                if (string.Equals(revisionId, obj.GetValueAsString(revisionIdAttributeName),
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    // Requested revision group, add to return collection
                    returnValue.Add(obj);
                }
            }

            // Return result
            return returnValue;
        }

        private static DataObject GetRevision(IEnumerable<DataObject> collection, string RevisionAttributeName,
            string revisionIdAttributeName, string revisionId, string ParentRevisionAttributeName,
            string parentRevision, bool isRoot, bool RootNullOrEmtpyRevision)
        {
            foreach (var dataObject in collection)
            {
                string revisionIdValule;
                try
                {
                    revisionIdValule = dataObject.GetValueAsString(revisionIdAttributeName);
                }
                catch (System.InvalidOperationException ex)
                {
                    throw new ApplicationException($"Unable to get revision group identifier because of missing attribute: {revisionIdAttributeName}", ex);
                }
                string parentRevisionValue;
                try
                {
                    parentRevisionValue = dataObject.GetValueAsString(ParentRevisionAttributeName);
                }
                catch (System.InvalidOperationException ex)
                {
                    throw new ApplicationException($"Unable to get parent revision value because of missing attribute: {ParentRevisionAttributeName}", ex);
                }
                string revisionValue;
                try
                {
                    revisionValue = dataObject.GetValueAsString(RevisionAttributeName);
                }
                catch (System.InvalidOperationException ex)
                {
                    throw new ApplicationException($"Unable to get revision value because of missing attribute: {RevisionAttributeName}", ex);
                }

                if (string.Equals(revisionIdValule, revisionId, StringComparison.InvariantCultureIgnoreCase))
                {
                    if ((isRoot && string.IsNullOrWhiteSpace(parentRevisionValue))
                            || string.Equals(parentRevisionValue, parentRevision, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Check whether to get a root revision and if the root revision should be null
                        if (isRoot && RootNullOrEmtpyRevision && string.IsNullOrWhiteSpace(revisionValue))
                        {
                            // Requested revision (empty root is requested, and root is empty)
                            return dataObject;
                        }
                        else if (isRoot && !RootNullOrEmtpyRevision && !string.IsNullOrWhiteSpace(revisionValue))
                        {
                            // Requested revision (no empty root is requested, and root is not empty)
                            return dataObject;
                        }
                        else if (!isRoot && string.Equals(parentRevisionValue, parentRevision, StringComparison.InvariantCultureIgnoreCase) &&
                              !string.IsNullOrWhiteSpace(revisionValue))
                        {
                            
                            // Requested revision (no root revision requested, and parent revision matches requested parent, since no root revision is requested the current
                            // revision value cannot be null (and therefore should not be null)
                            return dataObject;
                        }
                    }
                }
            }

            // Not found
            return null;
        }
    }
}