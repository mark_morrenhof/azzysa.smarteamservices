Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Infostrait.Azzysa.EV5ST.ConnectionPool")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("Infostrait.Azzysa.EV5ST.ConnectionPool")> 


<Assembly: ComVisible(False)>
<Assembly: AssemblyCopyright("Copyright © infostrait 2016")> 
<Assembly: AssemblyCompany("infostrait")> 
<Assembly: AssemblyTrademark("")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.3")> 
<Assembly: AssemblyFileVersion("2.0.0.3")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e504ff68-1cc0-482a-b4d0-2a0fc4b889c2")> 
