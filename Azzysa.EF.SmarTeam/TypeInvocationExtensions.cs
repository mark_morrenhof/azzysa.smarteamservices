﻿using System;
using System.Globalization;
using System.Reflection;
using Infostrait.Azzysa.EF.Common;
using SmarTeam.Std.Interop.SmarTeam.SmApplic;

namespace Azzysa.EF.SmarTeam
{
    public static class TypeInvocationExtensions
    {
        
        public static object DoCollectInvocationResult(this TypeInvocation invocation, ISmSession session, DataObject dataObject)
        {
            object[] constructorArgs;

            // Check type name to load, and load the type
            if (string.IsNullOrWhiteSpace(invocation.TypeName))
            {
                throw new ApplicationException("[DoCollectInvocationResult] : No type name!");
            }
            if (!invocation.TypeName.Contains(","))
            {
                throw new ApplicationException("[DoCollectInvocationResult] : Invalid type name, missing assembly/type ','!");
            }

            // Check for constructor arguments
            if (invocation.ConstructorParameters != null && invocation.ConstructorParameters.Count > 0)
            {
                // Use parameterized constructor
                constructorArgs = invocation.ConstructorParameters.GetValues(session, dataObject); ;
            } else {
                // Use parameterless constructor
                constructorArgs = null;
            }

            // Get type and assembly name
            var assemblyName = invocation.TypeName.Substring(0, invocation.TypeName.IndexOf(",", StringComparison.InvariantCultureIgnoreCase)).Trim();
            var typeName = invocation.TypeName.Substring(invocation.TypeName.IndexOf(",", StringComparison.InvariantCultureIgnoreCase) + 1).Trim();

            // Check both type and assembly name
            if (string.IsNullOrWhiteSpace(assemblyName)) { throw new ApplicationException($"Unable to get assembly name from mapping: {invocation.TypeName}"); }
            if (string.IsNullOrWhiteSpace(typeName)) { throw new ApplicationException($"Unable to get assembly name from mapping: {invocation.TypeName}"); }

            // Instantiate object
            object smarteamObject = null;
            try
            {
                smarteamObject = Activator.CreateInstance(assemblyName, typeName, true, BindingFlags.CreateInstance, null,
                constructorArgs, CultureInfo.CurrentCulture, null).Unwrap();
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Unexpected exception occured while loading '{typeName}' from assembly '{assemblyName}'", ex);
            }
            
            //  Iterate through instructions
            invocation.Instructions.Sort(new TypeInstructionComparator());
            if (invocation.Instructions != null && invocation.Instructions.Count > 0)
            {
                foreach (var instruction in invocation.Instructions)
                {
                    // Get parameters for method invocation
                    var methodArgs = instruction.Parameters.GetValues(session, dataObject);

                    // Invoke
                    smarteamObject.GetType().InvokeMember(instruction.MemberName, BindingFlags.InvokeMethod, null, smarteamObject, methodArgs);
                }
            }

            // Finished, return the created object
            return smarteamObject;
        }
    }
}