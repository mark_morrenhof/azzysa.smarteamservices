﻿using System;
using System.Linq;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public static class EntityMappingExtensions
    {

        public static bool IsValid(this EntityMapping entityMapping, DataObject dataObject)
        {
            if (string.IsNullOrWhiteSpace(entityMapping.EntityExpression) || dataObject == null)
            {
                // No expression to check
                return false;
            }
            else {
                // Check the expression
                var entityExpression = entityMapping.EntityExpression.Trim();
                if (entityExpression.StartsWith("attribute[", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Attribute expression
                    var attributeName = entityExpression.Substring(entityExpression.IndexOf("[", StringComparison.InvariantCultureIgnoreCase) + 1,
                        (entityExpression.IndexOf("]", StringComparison.InvariantCultureIgnoreCase) - entityExpression.IndexOf("[", StringComparison.InvariantCultureIgnoreCase)) - 1);

                    // Check if the attribute name is within the dataObject attribute collection
                    if (dataObject.Attributes.Exists(
                            p => string.Equals(p.Name, attributeName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        // Check the remaining part of the expression
                        var expression = entityExpression.Substring(entityExpression.IndexOf(attributeName, StringComparison.InvariantCultureIgnoreCase) + attributeName.Length);
                        expression = expression.Substring(expression.IndexOf("]", StringComparison.InvariantCultureIgnoreCase) + 1).Trim();

                        // Check the remaining expression
                        if (expression.StartsWith("==", StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Check attribute value
                            return string.Equals(dataObject.Attributes.Single(
                                p => string.Equals(p.Name, attributeName, StringComparison.InvariantCultureIgnoreCase))
                                .Value?.ToString() ?? string.Empty,
                                expression.Substring(expression.IndexOf("==", StringComparison.InvariantCultureIgnoreCase) + 2).Trim());
                        }
                        else if (expression.StartsWith(".hasvalue", StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Check if the attribute has a value
                            return !string.IsNullOrWhiteSpace(dataObject.Attributes.Single(
                                p => string.Equals(p.Name, attributeName, StringComparison.InvariantCultureIgnoreCase))
                                .Value as string);
                        }
                        else
                        {
                            // Unknown attribute expression
                            throw new ApplicationException($"Unknown attribute expression : {entityExpression} => {expression}");
                        }
                    }
                    else
                    {
                        // Attribute is not present at this data object, this entity mapping is therefore not valid for the specifed data object
                        return false;
                    }
                }
                else
                {
                    // Unknown expression
                    throw new ApplicationException($"Unknown expression: {entityExpression}");
                }
            }
        }
    }
}