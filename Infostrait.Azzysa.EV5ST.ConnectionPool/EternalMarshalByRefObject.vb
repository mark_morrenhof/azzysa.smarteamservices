Public Class EternalMarshalByRefObject
    Inherits MarshalByRefObject

    Public Overrides Function InitializeLifetimeService() As Object
        ' This is to insure that when created as a Singleton,
        ' the instance never dies, no matter how long between client calls.
        Return Nothing
    End Function
End Class