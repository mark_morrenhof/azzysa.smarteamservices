﻿using System;
using System.Linq;
using Infostrait.Azzysa.EF.Common;
using SmarTeam.Std.Interop.SmarTeam.SmApplic;

namespace Azzysa.EF.SmarTeam
{
    public static class DataObjectExtension
    {

        /// <summary>
        /// Function that actually imports the data object, using the mapping
        /// </summary>
        /// <returns></returns>
        public static ProcessResult Process(this DataObject dataObject, ISmSession session, Mapping mapping)
        {
            var result = new ProcessResult();
            var iValidMappings = 0;
            try
            {
                // Iterate through all entity mappings in the mapping object
                // For each mapping, the expression is checked to verify whether to apply this mapping on the current data object
                foreach (var entityMapping in mapping.EntityMappings)
                {
                    // Check expression
                    if (entityMapping.IsValid(dataObject))
                    {
                        // Increment valid mapping counter to make sure log when no valid mappings have been found
                        iValidMappings += 1;

                        // Is valid, continue processing by iterating through the invocation collection
                        foreach (var invocation in entityMapping.InvocationCollection)
                        {
                            // Invoke TypeInvocations in the collection
                            // Since the return value is not used (errors are thrown and handled by this try/catch block) do not process the return
                            // value of the 'invocation.DoCollectInvocationResult(session)' invocation
                            invocation.DoCollectInvocationResult(session, dataObject);
                        }
                    }
                }

                // Check the number of valid mappings
                if (iValidMappings == 0)
                {
                    // No valid mappings have been found, return warning message
                    result.Result = ProcessResult.eResult.Warning;
                    result.ExceptionMessage =
                        "[DataObjectExtension.Process]: No valid mapping expression for data object..";
                    result.ExceptionText =
                        "[DataObjectExtension.Process]: No valid mapping expression for data object..";
                }
                else
                {
                    // Mappings have been processed, no exception has occurs
                    result.Result = ProcessResult.eResult.Success;
                }

                // Finished
            }
            catch (Exception ex)
            {
                result.Result = ProcessResult.eResult.Error;
                result.ExceptionMessage = ex.Message;
                result.ExceptionText = ex.ToString();
            }

            // Return the result
            return result;
        }

        public static string GetValueAsString(this DataObject dataObject, string attributeName)
        {
            var val = dataObject.Attributes?.Single(a => a.Name == attributeName).Value;
            return (val == null ? string.Empty : val.ToString());
        }
    }
}