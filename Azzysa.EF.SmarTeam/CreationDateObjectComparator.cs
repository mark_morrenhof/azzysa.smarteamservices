﻿using System;
using System.Collections.Generic;
using Infostrait.Azzysa.EF.Common;

namespace Azzysa.EF.SmarTeam
{
    public class CreationDateObjectComparator : IComparer<DataObject>
    {
        private readonly string _creationDateAttributeName;

        public CreationDateObjectComparator(string creationDateAttributeName)
        {
            _creationDateAttributeName = creationDateAttributeName;
        }

        public int Compare(DataObject x, DataObject y)
        {
            var dateX = DateTime.Parse(x.GetValueAsString(_creationDateAttributeName));
            var dateY = DateTime.Parse(y.GetValueAsString(_creationDateAttributeName));
            return dateX.CompareTo(dateY);
        }
    }
}
