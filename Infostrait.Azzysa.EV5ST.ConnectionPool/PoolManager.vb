Imports System.Reflection
Imports System.Configuration.ConfigurationManager
Imports System.Threading


''' <summary>
''' Pool, manage and provide pooled objects (Connections)
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' Modified:         29 august 2006: removal of Exception and LanguageManager dependencies
''' Modified:         23 januari 2007: make the connection pool generic
''' Modified:         5 februari 2007: add a system object:
'''                       - pool for licensed objects (to provide webinterface users with the backend API, license based)
'''                       - one object to use by system services (e.g. transaction service, always available)
''' Modified:         6 februari 2007: removal of .NET dispose mechanism, since it doens't work ok in some/much cases
''' Modified:         5 may 2009: Redesign, new mechanism.
''' </history>
<Serializable()> Public Class PoolManager
    Inherits EternalMarshalByRefObject

    Private _objectPool As Dictionary(Of Integer, BasePoolObject)
    Private _connectionString As Dictionary(Of String, String)

    Private _creatingObject As Int32 = 0
    Private _initiatingPool As Int32 = 0

    ''' <summary>
    ''' 1=Pool is in panic state and cannot be recovered.
    ''' 0=Pool is not in panic state.
    ''' 
    ''' Note that pool panic is only set when the pool cannot be initiated.
    ''' </summary>
    ''' <remarks></remarks>
    Private _poolPanic As Int32 = 0

    ''' <summary>
    ''' Returns an AppDomain safe instance.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Public Shared ReadOnly Property Instance() As PoolManager
        Get
            Return _instance
        End Get
    End Property

    Private Shared _instance As New PoolManager

#Region "Starting up and creating the pool"
    Friend Sub New()
        _objectPool = New Dictionary(Of Integer, BasePoolObject)
    End Sub

    ''' <summary>
    ''' Initializes the pool. Creates the objects in the connection pool.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Friend Sub Initiate(connectionString As String)
        Try
            If Interlocked.CompareExchange(_initiatingPool, 1, 0) = 0 Then
                ' Convert string to dictionary
                _connectionString = ConnectionStringBuilder.ToDictionary(connectionString)

                Dim addSize = Me.GetPoolSize.Min

                For i As Integer = 0 To addSize - 1
                    Dim result = Me.AddObject()

                    If result = False Then
                        Throw New Exception("Initial creation of pool object failed.")
                    End If
                Next
            Else
                ' Pool is already initiating
            End If
        Catch ex As Exception
            If Me._objectPool.Count = 0 Then
                ' Error and no objects are available, set panic
                Interlocked.Exchange(_poolPanic, 1)
            End If

            Dim log = New EventLog("Azzysa")
            log.Source = "Azzysa"
            log.WriteEntry(ex.ToString(), EventLogEntryType.Error)

            Throw
        Finally
            Interlocked.Exchange(_initiatingPool, 0)
        End Try
    End Sub

    Private Function CreateObject() As BasePoolObject
        If Interlocked.CompareExchange(_creatingObject, 1, 0) = 0 Then
            Try
                Dim type = "smarteamclient"

                If _connectionString.ContainsKey("ConnectionType") Then
                    type = _connectionString("ConnectionType")
                End If

                Dim result As BasePoolObject

                Select Case type.ToLower
                    Case "smarteameditor"
                        result = New SmarTeamEditorPoolObject(_connectionString)
                    Case "smarteamwebeditor"
                        result = New SmarTeamWebEditorPoolObject(_connectionString)
                    Case "smarteamnavigator"
                        result = New SmarTeamNavigatorPoolObject(_connectionString)
                    Case "smarteamclient"
                        result = New SmarTeamClientPoolObject(_connectionString)
                    Case "none"
                        result = Nothing
                    Case "sqlserver"
                        ' SQL has it's own pool.
                        result = Nothing
                    Case Else
                        Throw New Exception("Specified connectiontype " & type & " is not supported.")
                End Select

                If result IsNot Nothing Then
                    If result.Initiate() Then
                        ' Clear pool panic, if set
                        Interlocked.Exchange(_poolPanic, 0)

                        result.IsAvailable = True
                        Return result
                    Else
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                Throw
            Finally
                Interlocked.Exchange(_creatingObject, 0)
            End Try
        Else
            Return Nothing
        End If
    End Function

    Private Class PoolSize
        Property Min As Integer
        Property Max As Integer
    End Class

    Private Function GetPoolSize() As PoolSize
        Dim ps = New PoolSize

        If _connectionString.ContainsKey("PoolSize") Then
            If _connectionString("PoolSize").Split("-"c).Length <= 1 Then
                ps.Min = Integer.Parse(_connectionString("PoolSize").Split("-"c)(0))
                ps.Max = ps.Min
            Else
                ps.Min = Integer.Parse(_connectionString("PoolSize").Split("-"c)(0))
                ps.Max = Integer.Parse(_connectionString("PoolSize").Split("-"c)(1))
            End If
        Else
            ps.Min = 1
            ps.Max = 1
        End If

        Return ps
    End Function
#End Region

#Region "Reserving pool objects"
    ''' <summary>
    ''' Get free object from pool, reserve pool object for use.
    ''' If no object is available, the function waits for an object to become available, with timeout if not.
    ''' </summary>
    ''' <param name="connectionString"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	19-9-2007	Created
    ''' </history>
    Public Function ReserveObject(connectionString As Dictionary(Of String, String)) As BasePoolObject
        Dim s As String = ConnectionStringBuilder.ToString(connectionString)

        ' 1) Try to get an object
        Dim o As BasePoolObject
        o = Me.TryReserveObject(connectionString)

        If Not o Is Nothing Then
            Return o
        Else
            ' 2) No object available, see if we are allowed to create a new object
            If Me.GetPoolSize.Max > Me._objectPool.Count And _initiatingPool = 0 Then
                Me.AddObject()
                o = Me.TryReserveObject(connectionString)
            End If

            If Not o Is Nothing Then
                Return o
            Else
                If _initiatingPool = 1 Then
                    ' 3a) Wait for next object available for 3 minutes. Try every 1 seconds.
                    For i As Integer = 0 To 180
                        o = Me.TryReserveObject(connectionString)

                        If Not o Is Nothing Then
                            Return o
                        Else
                            Thread.Sleep(TimeSpan.FromSeconds(1))
                        End If
                    Next
                Else
                    ' 3b) Wait for next object available for 30 seconds. Try every 0.25 seconds.
                    For i As Integer = 0 To 120
                        o = Me.TryReserveObject(connectionString)

                        If Not o Is Nothing Then
                            Return o
                        Else
                            Thread.Sleep(TimeSpan.FromSeconds(0.25))
                        End If
                    Next
                End If

                ' 4) Not able to return pool object in reasonable amount of time
                Throw New Exception("No pool object available, pool limit reached. Increase pool size using pool configuration (static) or using the pool library (dynamic).")
            End If
        End If
    End Function

    ''' <summary>
    ''' Try to get a free object from pool, reserve pool object for use.
    ''' Mechanism tries to return the best possible object.
    ''' If no object is available, Nothing is returned.
    ''' </summary>
    ''' <param name="connectionString"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    Private Function TryReserveObject(connectionString As Dictionary(Of String, String)) As BasePoolObject
        Me.CheckPoolPanic()

        Dim s As String = ConnectionStringBuilder.ToString(connectionString)
        Dim o As BasePoolObject

        ' 1) Find an object, according to the requirements (exact)
        Dim oldestAvailableObject As BasePoolObject = Nothing

        For Each pair In _objectPool
            o = pair.Value

            ' First do an unsafe quick match, to prevent unneeded interlocking of objects we cannot use 
            If o.Available = 1 AndAlso o.Session IsNot Nothing AndAlso o.QuickMatch(connectionString) Then
                ' If available=true, replace available with false, in an atomic operation (returns if the object was available)
                If Interlocked.CompareExchange(o.Available, 0, 1) = 1 Then
                    Try
                        o.Validate(ValidationMode.Quick)
                        If o.ExactMatch(connectionString) Then
                            o.SetLastUsed()
                            Return o
                        Else
                            ' Set available to true, in an atomic operation
                            Interlocked.Exchange(o.Available, 1)
                        End If
                    Catch ex As Exception
                        Interlocked.Exchange(o.Available, 1)

                        Throw
                    End Try
                End If
            End If

            If oldestAvailableObject Is Nothing Then
                oldestAvailableObject = o
            Else
                If o.LastUsed < oldestAvailableObject.LastUsed Then
                    oldestAvailableObject = o
                End If
            End If
        Next

        ' 2) Find an object, according to the requirements (try, modify state if needed, see if we can use the oldest object)
        If Not oldestAvailableObject Is Nothing Then
            o = oldestAvailableObject

            ' If available=true, replace available with false, in an atomic operation (returns if the object was available)
            If Interlocked.CompareExchange(o.Available, 0, 1) = 1 Then
                Try
                    o.Validate(ValidationMode.Quick)
                    If o.TryMatch(connectionString) Then
                        o.SetLastUsed()
                        Return o
                    Else
                        ' Set available to true, in an atomic operation
                        Interlocked.Exchange(o.Available, 1)
                    End If
                Catch ex As Exception
                    Interlocked.Exchange(o.Available, 1)

                    Throw
                End Try
            End If
        End If

        ' 3) Find an object, according to the requirements (try, modify state if needed, find the first one we can find)
        For Each key In _objectPool
            o = DirectCast(key.Value, BasePoolObject)

            ' If available=true, replace available with false, in an atomic operation (returns if the object was available)
            If Interlocked.CompareExchange(o.Available, 0, 1) = 1 Then
                Try
                    o.Validate(ValidationMode.Quick)
                    If o.TryMatch(connectionString) Then
                        o.SetLastUsed()
                        Return o
                    Else
                        ' Set available to true, in an atomic operation
                        Interlocked.Exchange(o.Available, 1)
                    End If
                Catch ex As Exception
                    Interlocked.Exchange(o.Available, 1)

                    Throw
                End Try
            End If
        Next

        Return Nothing
    End Function

    Private Sub CheckPoolPanic()
        If _poolPanic = 1 Then
            Throw New Exception("The pool is not properly initiated. Check the pool configuration or ask your system administrator for details.")
        End If
    End Sub
#End Region

#Region "Managing pool objects"
    Public Function ValidateObject(objectHashCode As Integer) As Boolean
        Dim o As BasePoolObject

        For Each pair In _objectPool
            o = pair.Value

            If o.GetHashCode() = objectHashCode Then
                ' If available=true, replace available with false, in an atomic operation (returns if the object was available)
                If Interlocked.CompareExchange(o.Available, 0, 1) = 1 Then
                    Try
                        o.Validate(ValidationMode.Full)
                    Catch ex As Exception
                        Throw
                    Finally
                        ' Set available to true, in an atomic operation
                        Interlocked.Exchange(o.Available, 1)
                    End Try
                End If

                Return True
            End If
        Next
        Return False
    End Function

    Public Sub ValidatePoolBounds()
        Dim terminationTimeOutInMinutes = 5

        If Not String.IsNullOrEmpty(AppSettings("PoolObjectTerminationTimeOutInMinutes")) Then
            terminationTimeOutInMinutes = Integer.Parse(AppSettings("PoolObjectTerminationTimeOutInMinutes"))
        End If

        ' Add pool objects if needed
        Dim addSize = (Me.GetPoolSize.Min - Me._objectPool.Count)

        For i As Integer = 0 To addSize - 1
            Dim result = Me.AddObject()

            If result = False Then
                Throw New Exception("Creation of pool object failed.")
            End If
        Next

        ' Remove pool objects if needed
        Dim terminatedObjects = New List(Of Integer)

        For Each pair In _objectPool
            Dim o = DirectCast(pair.Value, BasePoolObject)

            If (Me._objectPool.Count - terminatedObjects.Count) > Me.GetPoolSize.Min Then
                If o.LastUsed.AddMinutes(terminationTimeOutInMinutes) < Now Then
                    ' If available=true, replace available with false, in an atomic operation (returns if the object was available)
                    If Interlocked.CompareExchange(o.Available, 0, 1) = 1 Then
                        terminatedObjects.Add(pair.Key)
                        o.Terminate()
                    End If
                End If
            End If
        Next

        For Each key In terminatedObjects
            _objectPool.Remove(key)
        Next
    End Sub

    Public Sub ValidateObjects()
        Dim o As BasePoolObject

        For Each pair In _objectPool
            o = pair.Value

            Me.ValidateObject(o.GetHashCode)
        Next
    End Sub

    Public Function ResetObject(objectHashCode As Integer) As Boolean
        Dim o As BasePoolObject

        For Each pair In _objectPool
            o = pair.Value

            If o.GetHashCode() = objectHashCode Then
                Interlocked.Exchange(o.Available, 0)
                o.Terminate()
                o.Initiate()
                Interlocked.Exchange(o.Available, 1)
                Return True
            End If
        Next
        Return False
    End Function

    Public Function TerminateObject(objectHashCode As Integer) As Boolean
        Dim o As BasePoolObject = Nothing

        For Each pair In _objectPool
            o = pair.Value

            If o.GetHashCode() = objectHashCode Then
                Interlocked.Exchange(o.Available, 0)
                o.Terminate()
                Exit For
            End If
        Next

        If Not o Is Nothing Then
            SyncLock _objectPool
                _objectPool.Remove(o.GetHashCode)
            End SyncLock

            Return True
        Else
            Return False
        End If
    End Function

    Public Function FreeObject(objectHashCode As Integer) As Boolean
        Dim o As BasePoolObject

        For Each pair In _objectPool
            o = pair.Value

            If o.GetHashCode() = objectHashCode Then
                o.Validate(ValidationMode.Full)
                Interlocked.Exchange(o.Available, 1)
                Return True
            End If
        Next
        Return False
    End Function

    Public Function BlockObject(objectHashCode As Integer) As Boolean
        Dim o As BasePoolObject

        For Each pair In _objectPool
            o = pair.Value

            If o.GetHashCode() = objectHashCode Then
                Interlocked.Exchange(o.Available, 0)
                Return True
            End If
        Next
        Return False
    End Function

    Public Function AddObject() As Boolean
        Dim o As BasePoolObject
        o = Me.CreateObject()

        ' Check if the object is successfully created.
        If o IsNot Nothing Then
            SyncLock _objectPool
                _objectPool.Add(o.GetHashCode, o)
            End SyncLock

            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetPoolCount() As Integer
        Return _objectPool.Count
    End Function
#End Region

#Region "Terminate pool"
    ''' <summary>
    ''' Disconnect and terminate all objects, definitive.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[t.v.oost]	24-9-2007	Created
    ''' </history>
    Public Sub TerminatePool()
        Dim o As BasePoolObject

        For Each pair In _objectPool
            o = pair.Value

            If Not o Is Nothing Then
                Interlocked.Exchange(o.Available, 0)
                o.Terminate()
            End If
        Next

        For Each pair In _objectPool
            o = pair.Value

            If Not o Is Nothing Then
                Interlocked.Exchange(o.Available, 0)
                o.TerminateShared()
                Return
            End If
        Next
    End Sub
#End Region

#Region "Returning detailed information from the pool"
    ''' <summary>
    ''' Creates a report with information about the current state of the pool.
    ''' Returns information for every pool object.
    ''' </summary>
    ''' <returns>DataSet with information about pool objects</returns>
    ''' <remarks>
    ''' </remarks>
    Public Function GetPoolObjects() As DataSet
        Dim data As DataSet, table As DataTable
        data = New DataSet("Data")

        table = New DataTable("Result")
        table.Columns.Add(New DataColumn("OBJECT_ID", GetType(Integer)))
        table.Columns.Add(New DataColumn("CLASS_NAME", GetType(String)))
        table.Columns.Add(New DataColumn("IsAvailable", GetType(Boolean)))
        table.Columns.Add(New DataColumn("LastUsed", GetType(DateTime)))
        table.Columns.Add(New DataColumn("Created", GetType(DateTime)))
        table.Columns.Add(New DataColumn("Type", GetType(String)))

        For Each pair In _objectPool
            Dim o = pair.Value

            Dim row As DataRow
            row = table.NewRow()

            ' Add standard information
            row("OBJECT_ID") = o.GetHashCode
            row("CLASS_NAME") = "PoolObject"

            If o.Available = 1 Then
                row("IsAvailable") = True
            Else
                row("IsAvailable") = False
            End If

            row("LastUsed") = o.LastUsed
            row("Created") = o.Created
            row("Type") = o.GetType().Name.ToString

            ' Add other information from pool object
            For Each infoEntry In o.Information
                Dim infoValue = infoEntry.Value

                If Not infoValue Is Nothing Then
                    If Not table.Columns.Contains(infoEntry.Key.ToString) Then
                        table.Columns.Add(New DataColumn(infoEntry.Key.ToString, infoValue.GetType()))
                    End If

                    row(infoEntry.Key.ToString) = infoValue
                End If
            Next

            table.Rows.Add(row)
        Next

        data.Tables.Add(table)

        Return data
    End Function

    Private Shared _cachedPoolInfoAssemblies As String = String.Empty

    ''' <summary>
    ''' Creates a report with information about the pool.
    ''' Returns (configuration) information from the pool.
    ''' </summary>
    ''' <returns>DataSet with information about the pool</returns>
    ''' <remarks>
    ''' </remarks>
    Public Function GetPool() As DataSet
        Dim data As DataSet, table As DataTable
        data = New DataSet("Data")

        table = New DataTable("Result")
        table.Columns.Add(New DataColumn("OBJECT_ID", GetType(Integer)))
        table.Columns.Add(New DataColumn("CLASS_NAME", GetType(String)))
        table.Columns.Add(New DataColumn("PoolSize", GetType(String)))
        table.Columns.Add(New DataColumn("NumberOfPoolObjects", GetType(Integer)))
        table.Columns.Add(New DataColumn("Version", GetType(String)))
        table.Columns.Add(New DataColumn("ConnectionString", GetType(String)))
        table.Columns.Add(New DataColumn("Assemblies", GetType(String)))
        table.Columns.Add(New DataColumn("AvailableCount", GetType(Integer)))
        table.Columns.Add(New DataColumn("CreatingObject", GetType(Int32)))
        table.Columns.Add(New DataColumn("InitiatingPool", GetType(Int32)))
        table.Columns.Add(New DataColumn("PoolPanic", GetType(Int32)))

        Dim row As DataRow
        row = table.NewRow()
        row("OBJECT_ID") = 1
        row("CLASS_NAME") = "Pool"
        row("PoolSize") = Me.GetPoolSize.Min & "-" & Me.GetPoolSize.Max
        row("NumberOfPoolObjects") = Me._objectPool.Count
        row("Version") = [Assembly].GetExecutingAssembly().GetName().Version().ToString
        row("ConnectionString") = ConnectionStringBuilder.ToString(Me._connectionString)
        row("CreatingObject") = _creatingObject
        row("InitiatingPool") = _initiatingPool
        row("PoolPanic") = _poolPanic

        Dim assemblies As String = _cachedPoolInfoAssemblies
        If assemblies Is Nothing Or assemblies = String.Empty Then
            For Each a As [Assembly] In AppDomain.CurrentDomain.GetAssemblies()
                assemblies &= a.FullName & vbCrLf
            Next
            _cachedPoolInfoAssemblies = assemblies
        End If
        row("Assemblies") = assemblies

        Dim availableCount As Integer = 0
        For Each pair In _objectPool
            Dim o = pair.Value

            If o.Available = 1 Then
                availableCount = availableCount + 1
            End If
        Next
        row("AvailableCount") = availableCount

        table.Rows.Add(row)

        data.Tables.Add(table)

        Return data
    End Function
#End Region

End Class