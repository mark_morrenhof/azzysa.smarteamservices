﻿Public Class ConnectionStringBuilder
    Public Overloads Shared Function ToString(connectionString As Dictionary(Of String, String)) As String
        Dim result = String.Empty

        For Each entry In connectionString
            result &= entry.Key.ToString & "=" & entry.Value.ToString & ";"
        Next

        Return result
    End Function

    Public Shared Function ToDictionary(connectionString As String) As Dictionary(Of String, String)
        Dim result As Dictionary(Of String, String)
        result = New Dictionary(Of String, String)

        If Not String.IsNullOrWhiteSpace(connectionString) Then
            For Each s As String In connectionString.Split(";"c)
                If Not String.IsNullOrWhiteSpace(s) Then
                    Dim key = s.Split("="c)(0)

                    If Not result.ContainsKey(key) Then
                        If s.Split("="c).Length > 1 Then
                            result.Add(key, s.Split("="c)(1))
                        Else
                            result.Add(key, "")
                        End If
                    End If
                End If
            Next
        End If

        Return result
    End Function
End Class
